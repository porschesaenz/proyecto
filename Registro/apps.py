from django.apps import AppConfig


class RegistroConfig(AppConfig):
    name = 'Registro'
    verbose_name = 'Registros'
