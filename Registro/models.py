from django.db import models
from django.contrib.auth.models import User

# Create your models here.
#Modelo extendido o modelo proxy para el uso del modulo admin de django
#Una clase abstracta es una clase que contiene un model (el model del objeto que quiere recibir)
#y que sus valores no existen, lo unico que hacen es ser propiedades y pueden heredarse segun sea el caso.
#Una clase abstracta no puede modificarse su estructura y todos los campos se vuelven obligatorios.
#User.

class UsuarioRegistrado(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=9, blank=True)
    foto = models.ImageField(upload_to="Registro/imagenes", blank=True, null=True)
    about = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True) #Auto de la hora exacta que se creo en ese momento
    modified = models.DateTimeField(auto_now=True) #Este sola toma la hora exacta y la fecha

    def __str__(self):
        return self.user.username

class MercantilRegistro(models.Model):
    estados = (
        ('Soltero/a', 'Soltero/a'),
        ('Casado/a', 'Casado/a'),
        ('Divorciado/a', 'Divorciado/a'),
        ('Viudo/a', 'Viudo/a'),
    )
    opciones = (
        ('1) Inscribirse como comerciante individual', '1) Incribirse como comerciante individual'),
        ('2) Inscribir empresa de su propiedad', '2) Inscribir empresa de su propiedad'),
        ('3) Inscribir empresa de terceros', '3) Inscribir empresa de terceros'),
    )

    nombre = models.CharField(max_length=60, blank=False)
    edad = models.CharField(max_length=2, blank=False)
    estado_civil = models.CharField(max_length=20, choices = estados, blank=False)
    nacionalidad = models.CharField(max_length=20, blank=False)
    profesion = models.CharField(max_length=20, blank=False)
    direccion = models.CharField(max_length=50, blank=False)
    DPI = models.CharField(max_length=13, unique=True, blank=False)
    NIT = models.CharField(max_length=10, unique=True, blank=False)
    telefono = models.CharField(max_length=8, blank=False)
    email = models.EmailField(unique=True, blank=False)
    pido = models.CharField(max_length=50, choices = opciones, blank=False)
    empresa_terceros = models.CharField(max_length=30, verbose_name='Nombre de la empresa de terceros (Si selecciono opcion 3 anteriormente)', blank=True)
    nombre_comercial = models.CharField(max_length=30, unique=True, blank=False)
    direccion_comercial = models.CharField(max_length=30, blank=False)
    objeto = models.TextField()
    fecha_inicio = models.DateField(blank=False, null=False, default=None)
    capital = models.CharField(max_length=10, blank=False)