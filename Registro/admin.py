from django.contrib import admin
from Registro.models import UsuarioRegistrado
from Registro.models import MercantilRegistro

# Register your models here.
# Importancion de manera larga o usar un decorador

@admin.register(UsuarioRegistrado) #Esto es un decorador

class UsuarioRegistradoAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone', 'about')

class MercantilRegistroAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'DPI', 'telefono')

admin.site.register(MercantilRegistro) #Forma larga de importar el modelo