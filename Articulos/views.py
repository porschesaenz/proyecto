from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime

articulos = [
    {
        'nombre' : 'Generador de Onda',
        'codigo' : 'G01',
        'imagen' : 'https://www.instrumentacionhoy.com/imagenes/2013/07/TFG-3605E.jpg',
        'fecha' : datetime.now().strftime('%b %d %Y - %H:%M')
    },
    {
        'nombre' : 'Osciloscopio',
        'codigo' : 'O01',
        'imagen' : 'http://www.lopacan.es/WebRoot/hostaliawl/Shops/2804864/5845/4EA6/E6FA/E0E4/A7FF/7F00/0001/74AE/2190D.jpg',
        'fecha' : datetime.now().strftime('%b %d %Y - %H:%M')
    }
]
# Create your views here.
def lista_articulos(request):
    content = []    
    for item in articulos:
        content.append("""
            <p><strong>{nombre}</strong></p>
            <p><small>{codigo} - <i>{fecha}</i></small></p>
            <figure><img src="{imagen}"/></figure>
        """.format(**item))
    
    return HttpResponse('<br>'.join(content)) 

def lista_articulos2(request):
    content = []    
    for item in articulos:
        content.append("""
            <p><strong>{nombre}</strong></p>
            <p><small>{codigo} - <i>{fecha}</i></small></p>
            <figure><img src="{imagen}"/></figure>
        """.format(**item))
    return render(request, "articulos.html", {"lista": articulos})
