from django.db import models

# Create your models here.
class User(models.Model):
    nombre_usuario = models.CharField(max_length=40)
    apellido = models.CharField(max_length=40)
    email = models.EmailField(unique=True) #unique para registrar el correo de una persona solo una vez en el sistema
    username = models.CharField(max_length=12)
    descripcion = models.TextField()
    isAdmin = models.BooleanField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)

class Nacimientos(models.Model):
    nombre_nacido = models.CharField(max_length=50)
    nombre_padre = models.CharField(max_length=50)
    nombre_madre = models.CharField(max_length=50)
    fecha = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    
