from django.http import HttpResponse
import json

def ordernar_numeros(request):
    lista_numeros = request.GET['array'] #Recibimos el objeto
    lista_numeros = lista_numeros.split(',') #Separamos los elementos
    lista_numeros = [int(index) for index in lista_numeros] # Recorremos el objeto con el objetivo  
    # de que lista_numeros se convierta en una variable de tipo lista
    data = {
        'status' : 'ok',
        'responde' : 'array de numeros',
        'clase' : 'Proyectos 980',
        'array_numeros' : sorted(lista_numeros), #Sorted sirve para ordenar 
        'mensaje' : 'Proceso Terminado',
    }
    return HttpResponse(json.dumps(data), content_type=("application/json")) #Mandarle el tipo de aplicacion, puede ser
    #html, json, pdf y nos sirve para visualizar los datos como nosotros necesitamos

def saludo(request):
    return HttpResponse